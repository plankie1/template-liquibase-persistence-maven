/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.template.user;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import za.co.template.util.LoggingUtil;

/**
 *
 * @author Dante
 */
@Stateless

public class UserService {
	@PersistenceContext
	private EntityManager em;

	public static final String className = UserService.class.getSimpleName();

	public boolean createUser(User u) {
		LoggingUtil.methodEnter(className + ": createUser()");
		try {
			em.persist(u);
			em.flush();
		} catch (PersistenceException pe) {
			LoggingUtil.error(className + ": Persistence error: " + pe);
			return false;
		} catch (Exception e) {
			LoggingUtil.error(className + ": Persistence error: " + e);
			return false;
		}
		LoggingUtil.methodExit(className + ": createUser()");
		return true;
	}
}
