package za.co.template.util;


import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoggingUtil {
	final static Logger logger = LogManager.getLogger(LoggingUtil.class.getSimpleName());

	public static void methodEnter(String msg){

		logger.trace("Method Entered: " + msg);
	}

	public static void methodEnter(String msg, Object obj){

		logger.trace("Method Entered: " + msg + " values: " + ReflectionToStringBuilder.toString(obj));
	}

	public static void trace(String msg){

		logger.trace(msg);
	}

	public static void methodExit(String msg){

		logger.trace("Method Returned: " + msg);
	}

	public static void info(String msg){

		logger.info("Method Returned: " + msg);
	}

	public static void error(String msg){

		logger.error("Method Returned: " + msg);
	}
}
