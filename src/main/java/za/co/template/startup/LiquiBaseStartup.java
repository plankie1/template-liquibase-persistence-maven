package za.co.template.startup;

import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.sql.DataSource;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.resource.ResourceAccessor;
import za.co.template.user.UserService;
import za.co.template.util.LoggingUtil;

@Startup
@Singleton
@TransactionManagement(TransactionManagementType.BEAN)
public class LiquiBaseStartup {

	public static final String className = LiquiBaseStartup.class.getSimpleName();
	private static final String STAGE = "development";
	private static final String CHANGELOG_FILE = "liquibase/changelog.xml";

	@Resource(lookup = "jdbc/templatedb")
	DataSource ds;
	
	@Inject
	UserService userService;

	@PostConstruct
	protected void init() {
		//BasicConfigurator.configure();
		LoggingUtil.methodEnter(className + ": init()");
		ResourceAccessor resourceAccessor = new ClassLoaderResourceAccessor(getClass().getClassLoader());
		try (Connection connection = ds.getConnection()) {
			JdbcConnection jdbcConnection = new JdbcConnection(connection);
			Database db = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);

			Liquibase liquiBase = new Liquibase(CHANGELOG_FILE, resourceAccessor, db);
			liquiBase.update(STAGE);
		} catch (SQLException | LiquibaseException e) {
			LoggingUtil.error(e.getMessage());
		}
		LoggingUtil.methodExit(className + ": init()");
		

	}
}
