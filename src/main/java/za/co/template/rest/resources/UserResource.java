/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.template.rest.resources;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import za.co.template.user.User;
import za.co.template.user.UserService;

/**
 *
 * @author Dante
 */
@Path("/user")
public class UserResource {

	@Inject
	UserService userService;
	
	
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	@Path("/createUser")
	public Response createUser() {
		User u = new User("Louis");
		boolean success = userService.createUser(u);
		return Response.ok(u).build();
	}

}
